def getWeights():
#    weight_list = ['hdamp_0p5mtop_NNPDF', 'hdamp_mtop_NNPDF', 'hdamp_1p5mtop_NNPDF', 'hdamp_2mtop_NNPDF', 'hdamp_3mtop_NNPDF', 'hdamp_4mtop_NNPDF', 'hdamp_5mtop_NNPDF', 'hdamp_0p5mtop_MMHT', 'hdamp_mtop_MMHT', 'hdamp_1p5mtop_MMHT', 'hdamp_2mtop_MMHT', 'hdamp_3mtop_MMHT', 'hdamp_4mtop_MMHT', 'hdamp_5mtop_MMHT', 'hdamp_0p5mtop_CT14', 'hdamp_mtop_CT14', 'hdamp_1p5mtop_CT14', 'hdamp_2mtop_CT14', 'hdamp_3mtop_CT14', 'hdamp_4mtop_CT14', 'hdamp_5mtop_CT14', 'hdamp_0p5mtop_PDF4LHC', 'hdamp_mtop_PDF4LHC', 'hdamp_1p5mtop_PDF4LHC', 'hdamp_2mtop_PDF4LHC', 'hdamp_3mtop_PDF4LHC', 'hdamp_4mtop_PDF4LHC', 'hdamp_5mtop_PDF4LHC', 'hdamp_0p5mtop_2muF', 'hdamp_0p5mtop_0p5muF', 'hdamp_0p5mtop_2muR', 'hdamp_0p5mtop_0p5muR', 'hdamp_0p5mtop_0p5muF_0p5muR', 'hdamp_0p5mtop_2muF_2muR', 'hdamp_0p5mtop_0p5muF_2muR', 'hdamp_0p5mtop_2muF_0p5muR', 'hdamp_mtop_2muF', 'hdamp_mtop_0p5muF', 'hdamp_mtop_2muR', 'hdamp_mtop_0p5muR', 'hdamp_mtop_0p5muF_0p5muR', 'hdamp_mtop_2muF_2muR', 'hdamp_mtop_0p5muF_2muR', 'hdamp_mtop_2muF_0p5muR', 'hdamp_1p5mtop_2muF', 'hdamp_1p5mtop_0p5muF', 'hdamp_1p5mtop_2muR', 'hdamp_1p5mtop_0p5muR', 'hdamp_1p5mtop_0p5muF_0p5muR', 'hdamp_1p5mtop_2muF_2muR', 'hdamp_1p5mtop_0p5muF_2muR', 'hdamp_1p5mtop_2muF_0p5muR', 'hdamp_2mtop_2muF', 'hdamp_2mtop_0p5muF', 'hdamp_2mtop_2muR', 'hdamp_2mtop_0p5muR', 'hdamp_2mtop_0p5muF_0p5muR', 'hdamp_2mtop_2muF_2muR', 'hdamp_2mtop_0p5muF_2muR', 'hdamp_2mtop_2muF_0p5muR', 'hdamp_3mtop_2muF', 'hdamp_3mtop_0p5muF', 'hdamp_3mtop_2muR', 'hdamp_3mtop_0p5muR', 'hdamp_3mtop_0p5muF_0p5muR', 'hdamp_3mtop_2muF_2muR', 'hdamp_3mtop_0p5muF_2muR', 'hdamp_3mtop_2muF_0p5muR', 'hdamp_4mtop_2muF', 'hdamp_4mtop_0p5muF', 'hdamp_4mtop_2muR', 'hdamp_4mtop_0p5muR', 'hdamp_4mtop_0p5muF_0p5muR', 'hdamp_4mtop_2muF_2muR', 'hdamp_4mtop_0p5muF_2muR', 'hdamp_4mtop_2muF_0p5muR', 'hdamp_5mtop_2muF', 'hdamp_5mtop_0p5muF', 'hdamp_5mtop_2muR', 'hdamp_5mtop_0p5muR', 'hdamp_5mtop_0p5muF_0p5muR', 'hdamp_5mtop_2muF_2muR', 'hdamp_5mtop_0p5muF_2muR', 'hdamp_5mtop_2muF_0p5muR']
#    weight_list = ['hdamp_0p5mtop_NNPDF', 'hdamp_mtop_NNPDF', 'hdamp_1p5mtop_NNPDF']

    weight_list = ['nominal',
#                   '2muF_NNPDF',
#                   '0p5muF_NNPDF',
#                   '2muR_NNPDF',
#                   '0p5muR_NNPDF',
#                   '0p5muF_0p5muR_NNPDF',
#                   '2muF_2muR_NNPDF',
#                   '0p5muF_2muR_NNPDF',
#                   '2muF_0p5muR_NNPDF'
]
    return weight_list
