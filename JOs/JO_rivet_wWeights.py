import os
import getWeights
theApp.EvtMax = -1

from AthenaCommon.AppMgr import ServiceMgr as svcMgr


import AthenaPoolCnvSvc.ReadAthenaPool

svcMgr.EventSelector.InputCollections=['/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/MC/TOP_EVNT/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/EVNT.06550123._000001.pool.root.1']
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

weight_list = getWeights.getWeights()


OutputRoot  = "<root_filename>"
OutputYoda  = "<yoda_filename>"

from Rivet_i.Rivet_iConf import Rivet_i

for w in weight_list:
    rivet = Rivet_i("Rivet_"+str(w.replace(".","_")))
    rivet.AnalysisPath = os.environ['PWD']

#rivet.Analyses +=["BoostedDiffXsec"]
#rivet.Analyses +=["MC_TTbar_TruthSel"]
#rivet.Analyses += ["ATLAS_2014_I1304289"]
#rivet.Analyses += ["ATLAS_2015_I1345452"]

    rivet.Analyses += ["Resolved13TeVljets"]
#rivet.Analyses += ["ATLAS_2014_I1304688"]
#rivet.Analyses += ["ATLAS_2013_I1243871"]

#rivet.Analyses += ['TTZ_analysis','hepmc_analysis','TTBAR_ANA']
#rivet.Analyses += [ 'MC_JET','PDFS','GENERIC','PHOTONS','PHOTONINC','WINC','TTBAR_ANA','HFJET_ANA','ZINC','JETTAGS']
#rivet.Analyses+=["TTBAR_ANA"]
    rivet.HistoFile = OutputYoda+str(w)+".yoda"
    rivet.DoRootHistos = False
    rivet.WeightName = " "+w+" "
# Specify MC cross section in pb (if necessary for your analysis)
    rivet.CrossSection = xs
    print '#####################'
    print '####  USERINPUT  ####'
    print '### >> : Cross-section -- ' + str(xs) + ' [pb]'
    print '#####################'
    job += rivet

#from GaudiSvc.GaudiSvcConf import THistSvc
#svcMgr += THistSvc()
#svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
