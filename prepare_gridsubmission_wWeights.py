#!/bin/python
import os,re,sys
import getWeights

path = os.environ['PWD']
Xsection = 831.76 # for top quark mass of 172.5 GeV (see https://twiki.cern.ch/twiki/bin/view/LHCPhysics/TtbarNNLO#Top_quark_pair_cross_sections_at)
Samples = {
#    'mc12_8TeV.117050.PowhegPythia_P2011C_ttbar.evgen.EVNT.e1727/': 114.49
#    'mc15_valid.429400.PowhegPythia8EvtGen_A14_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#    'mc15_valid.429401.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad.evgen.EVNT.e5042/':  451.64568,
#    'mc15_valid.429402.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad.evgen.EVNT.e5042/':  451.64568,
#    'mc15_valid.429403.PowhegPythia8EvtGen_Monash_ttbar_nonallhad.evgen.EVNT.e5042/':  451.64568,
#    'mc15_valid.429405.PowhegPythia8EvtGen_ATTBAR_ttbar_nonallhad.evgen.EVNT.e5042/':  451.64568,
#    'mc15_valid.429406.PowhegPythia8EvtGen_A14CMS_ttbar_nonallhad.evgen.EVNT.e5042/':  451.64568,
#    'mc15_valid.429407.PowhegPythia8EvtGen_A14v3cUpCMS_ttbar_nonallhad.evgen.EVNT.e5042/':  451.64568,
#    'mc15_valid.429408.PowhegPythia8EvtGen_A14v3cDoCMS_ttbar_nonallhad.evgen.EVNT.e5042/':  451.64568,
#    'mc15_valid.429404.PowhegPythia8EvtGen_MonashStar_ttbar_nonallhad.evgen.EVNT.e5066/':  451.64568,
#    'mc15_valid.429409.PowhegPythia8EvtGen_MonashCMS_ttbar_nonallhad.evgen.EVNT.e5066/':  451.64568,
#    'mc15_valid.429410.PowhegPythia8EvtGen_MonashStarCMS_ttbar_nonallhad.evgen.EVNT.e5066/': 451.64568
#    'mc15_valid.429411.PowhegPythia8EvtGen_ATTBARCMS_ttbar_nonallhad.evgen.EVNT.e5042/':  451.64568
#from list
#'mc15_valid.429400.PowhegPythia8EvtGen_A14_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#'mc15_valid.429401.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#'mc15_valid.429402.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#'mc15_valid.429407.PowhegPythia8EvtGen_A14v3cUpCMS_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#'mc15_valid.429408.PowhegPythia8EvtGen_A14v3cDoCMS_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,


##Nominal
#'mc15_valid.429403.PowhegPythia8EvtGen_Monash_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#'mc15_valid.429404.PowhegPythia8EvtGen_MonashStar_ttbar_nonallhad.evgen.EVNT.e5066/': 451.64568,
#'mc15_valid.429405.PowhegPythia8EvtGen_ATTBAR_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#'mc15_valid.429406.PowhegPythia8EvtGen_A14CMS_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#'mc15_valid.429409.PowhegPythia8EvtGen_MonashCMS_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,
#'mc15_valid.429410.PowhegPythia8EvtGen_MonashStarCMS_ttbar_nonallhad.evgen.EVNT.e5066/': 451.64568,
#'mc15_valid.429411.PowhegPythia8EvtGen_ATTBARCMS_ttbar_nonallhad.evgen.EVNT.e5042/': 451.64568,



#TopModelling studies
'mc15_valid.429430.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_0p5mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429431.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad_hdamp_0p5mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429432.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad_hdamp_0p5mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429433.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429434.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad_hdamp_mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429435.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad_hdamp_mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429436.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_1p5mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429437.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad_hdamp_1p5mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429438.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad_hdamp_1p5mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429439.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_2mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429440.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad_hdamp_2mt.evgen.EVNT.e5258/' : 451.64568,
'mc15_valid.429441.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad_hdamp_2mt.evgen.EVNT.e5258/' : 451.64568,

#'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/': 1,
#'mc15_valid.429430.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_0p5mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429431.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad_hdamp_0p5mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429432.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad_hdamp_0p5mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429433.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429434.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad_hdamp_mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429435.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad_hdamp_mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429436.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_1p5mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429437.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad_hdamp_1p5mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429438.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad_hdamp_1p5mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429439.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_2mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429440.PowhegPythia8EvtGen_A14v3cUp_ttbar_nonallhad_hdamp_2mt.evgen.EVNT.e5258/' : 1,
#'mc15_valid.429441.PowhegPythia8EvtGen_A14v3cDo_ttbar_nonallhad_hdamp_2mt.evgen.EVNT.e5258/' : 1,



    }

#filesToInclude = []
#includedir = "/include"
#for rivetfile in os.listdir(path+includedir):
#    filesToInclude.append(includedir+"/"+rivetfile)
filesToInclude = [
    #MC_TTbar_TruthSel
#    'RivetMC_BoostedDiffXsec.so'
    'RivetMC_Resolved13TeVljets.so',
    'Resolved13TeVljets.yoda'
#'RivetMC_ATLAS_2015_I1404878.so',
#'ATLAS_2015_I1404878.yoda'
#    'RivetMC_ResolvedDiffXsec.so',
#    'Resolved13TeVljets.yoda'
#    'RivetMC_TTbar_TruthSel.so',
#    'Steering_ATLAS_2014_I1304289',
#    'RivetMC_ATLAS_2014_I1304289.so',
#    'ATLAS_2014_I1304289.yoda',
#    'RivetMC_ATLAS_2015_I1345452.so',
#    'ATLAS_2015_I1345452.yoda'
    ]

#path = "."

UserName = os.environ['USER']
#UserName = 'sthenkel'

Version = 'TopTheory13TeVResRightXsec'
JOs = path+'/JOs/JO_rivet_wWeights.py'

#returns a list of weights
weight_list = getWeights.getWeights()

#athenaRel = '19.2.1.3,slc6'
#cmtConf = "x86_64-slc6-gcc47-opt"

FilesPerJob = 1
GBperJob = 'MAX'
script_download = open("../download/download_jobs.sh","w")
script_download.write("#!/bin/bash \n")


commands = []
RegEx = re.compile( '(\w*)\.(\d*)\.(\w*)\.(\w*)\.(\w*)\.(\w*)/' )

for sample in Samples:
    Match = RegEx.match( sample )
    outSample = 'user.' + UserName + '.' + Version + '.' + Match.group( 2 ) + '.' + Match.group( 3 ) +'.' + Match.group( 5 ) + '.' + Match.group( 6 ) + '/'

    output_yoda = Match.group(2) + '_' +Match.group(6) +'_'
    output_root = Match.group(2) + '_' +Match.group(6) +'_'
    counter = 0
    output_multiple = "*.yoda"
#    for w in weight_list:
#        counter += 1
#        if counter == len(weight_list):
#            output_multiple += output_yoda+str(w)+".yoda"
#        else:
#            output_multiple += output_yoda+str(w)+".yoda,"


    # JO handling and adjustment for each sample
    os.system("cp -r "+JOs+" "+path+"/JO_tmp.py")
    updated_JOs = path+"/JO_tmp.py"
    inf = open(updated_JOs).read()
    outf = open(updated_JOs, 'w')
    replacements = {'<yoda_filename>': output_yoda, '<root_filename>': output_root}

    for i in replacements.keys():
        inf = inf.replace(i, replacements[i])
    print 'In the JOs : '+updated_JOs+' replacing : '+str(i)+' with : '+str(replacements[i])
    outf.write(inf)
    outf.close

    (prefix, sep, suffix) = ("JO_tmp.py").rpartition('_')
    new_JOs = prefix + '_'+ Match.group(2) + '_' +Match.group(6) + '.py'
    os.system("mv JO_tmp.py "+new_JOs)

    # get files to include
    includeFiles = ','.join(filesToInclude)

    Xsec=Samples[sample]
    print '######## >> : '+str(Xsec)
    # Defining command and prepare sending the job
    print '###############################################################################'
    print '### >>> sending job for / preparing command : '+outSample
    print '###############################################################################\n'


    command = 'pathena -c \'xs = %f\' \'%s\' --extFile=\'%s\'  --nFilesPerJob=\'%s\' --long  --extOutFile=\'%s\' --inDS=\'%s\' --outDS=\'%s\' ' % ( Xsec, new_JOs, includeFiles, FilesPerJob, output_multiple, sample, outSample)


    # create submission script
    submissionscript="sub_"+Match.group(2) + '_' +Match.group(6)+".sh"
    script_sub = open(submissionscript,"w")
    #       script.write("source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh "+str(athenaVersion)+", here \n")
    script_sub.write("export RIVET_REF_PATH=$PWD \n")
    script_sub.write( command + "\n")
    script_sub.close()
    (file_d, sep_d, suffix_d) = outSample.rpartition('/')
    # prepare download script to download files once done on eht grid
    script_download.write( "rucio download --ndownloader 5 " + file_d +"_EXT0/"+"\n")
#    script_download.write( "rucio download " + file_d +"_Rivet/"+"\n")


    print '###########################################################'
    print command
#    os.system(command)
    print '###########################################################'
    print '#### >>> Spit out submission script : '+str(submissionscript)
    print '###########################################################\n'
 #   print '###############################################################################'
 #   print '### >>> command : '+command
 #   print '###############################################################################\n'
 #   os.system( command )
    commands.append(command)

script_download.close()
print '###############################################\n'
print '#### >>> Spit out download script to ./downloads folder '

subscript="Finalsub_"+str(Version)+"_jobs.sh"
script_sub = open(subscript,"w")
script_sub.write("#!/bin/bash \n")
#script_sub.write("setupATLAS \n")
#script_sub.write("asetup 19.2.5.5,slc6,64,here \n")
script_sub.write("voms-proxy-init -voms atlas \n")
script_sub.write("lsetup panda \n")
script_sub.write("export RIVET_REF_PATH=$PWD \n")
for com in commands:
    script_sub.write( com + "\n")
script_sub.close()
print '#### >>> Spit out final submission script ('+str(subscript)+') with all commands '
print '#### Execute, $> . '+str(subscript)+''
#    execfile("source "+path+"/sub_"+Match.group(2) + '_' +Match.group(6)+".sh")
#    os.system("chmod 755 batchscripts/"+str(jobname)+".lsf")
#execfile("source "+path+"/sub_410000_e3698.sh")
#execfile("source "+path+"/sub_"+Match.group(2) + '_' +Match.group(6)+".sh")
#    os.system("source sub_"+Match.group(2) + '_' +Match.group(6)+".sh")
#pathena --noBuild --extFile=RivetMC_TTbar_TruthSel.so,RivetMC_ATLAS_2014_I1304289.so,Steering_TTbar_Truth --nJobs=1 --nFilesPerJob=2 --long --extOutFile=Output_test.yoda --inDS=mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/ --outDS=user.sthenkel.rivet_test_wSteer3.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.EVNT.e3698/ JO.py


#    command = 'pathena %s --athenaTag=%s --cmtConfig=%s --nJobs=%d --long --nFilesPerJob=%d --nGBPerJob=%s --inDS=%s --outDS=%s --extFile=%s' % (JOs, athenaRel, cmtConf, nJobs, FilesPerJob, GBperJob, sample, outSample, OwnRivetAnalysis)
