#!/bin/bash 
setupATLAS 
asetup 19.2.5.5,slc6,64,here 
voms-proxy-init -voms atlas 
lsetup panda 
export RIVET_REF_PATH=$PWD 
pathena -c 'xs = 451.645680' 'JO_429400_e5043.py' --extFile='RivetMC_Resolved13TeVljets.so,Resolved13TeVljets.yoda' --nJobs='1'  --nFilesPerJob='100000' --long  --extOutFile='429400_e5043_' --inDS='mc15_valid.429400.PowhegPythia8EvtGen_A14_ttbar_nonallhad.evgen.EVNT.e5043/' --outDS='user.sthenkel.TopTheory13TeVTest.429400.PowhegPythia8EvtGen_A14_ttbar_nonallhad.EVNT.e5043/' 
