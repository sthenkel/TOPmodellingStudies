#!/bin/bash 
setupATLAS 
asetup 19.2.5.5,slc6,64,here 
voms-proxy-init -voms atlas 
lsetup panda 
pathena -c 'xs = 451.645680' 'JO_410000_e3698.py' --extFile='RivetMC_ATLAS_2015_I1404878.so,ATLAS_2015_I1404878.yoda' --nJobs='1'  --nFilesPerJob='100000' --long  --extOutFile='410000_e3698.yoda' --inDS='mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/' --outDS='user.sthenkel.13TeVDiffXsecResV-0-2.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.EVNT.e3698/' 
