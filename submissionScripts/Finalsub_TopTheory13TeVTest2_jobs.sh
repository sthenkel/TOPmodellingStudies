#!/bin/bash
lsetup panda
export RIVET_REF_PATH=$PWD
pathena -c 'xs = 451.645680' 'JO_429400_e5043.py' --extFile='RivetMC_Resolved13TeVljets.so,Resolved13TeVljets.yoda' --nJobs='1'  --nFilesPerJob='1' --nEventsPerFile='400'  --long  --extOutFile='*.yoda' --inDS='mc15_valid.429400.PowhegPythia8EvtGen_A14_ttbar_nonallhad.evgen.EVNT.e5043/' --outDS='user.sthenkel.TopTheory13TeVTest11.429400.PowhegPythia8EvtGen_A14_ttbar_nonallhad.EVNT.e5043/'
