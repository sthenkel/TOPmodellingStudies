#!/bin/bash
#setupATLAS
#asetup 19.2.5.5,slc6,64,here
voms-proxy-init -voms atlas
lsetup panda
export RIVET_REF_PATH=$PWD
pathena -c 'xs = 451.645680' 'JO_410000_e3698.py' --extFile='RivetMC_Resolved13TeVljets.so,Resolved13TeVljets.yoda' --nJobs='1'  --nFilesPerJob='100000' --long  --extOutFile='410000_e3698.yoda' --inDS='mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/' --outDS='user.sthenkel.13TeVDiffXsecResolvedSanityCv2.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.EVNT.e3698/'
