CurrentDIR=$(pwd)
DIR=$PWD
etag="e5066"

for subdir in $(find $PWD -mindepth 1 -maxdepth 1 -type d); do
  echo merging yodafiles in ${subdir}
  for weight in 0p5muF_0p5muR_NNPDF 0p5muF_2muR_NNPDF 0p5muF_NNPDF 0p5muR_NNPDF 2muF_0p5muR_NNPDF 2muF_2muR_NNPDF 2muF_NNPDF 2muR_NNPDF nominal
  do
      echo 'INFO :: Preparing weight -- '$weight
#      echo `find ${subdir} -type f -printf '%f\n' | grep $weight`
      declare -i nfiles=$(ls ${subdir}/out_*_${etag}_${weight}.yoda 2> /dev/null | wc -w)
#      echo 'DEBUG :: '$nfiles
    if [[ ${nfiles} != 0 ]]; then
      echo merging ${nfiles} \"${weight}\" yoda files in ${subdir}
#      command="yodamerge -o ${subdir}_out_${weight}.yoda $(ls ${subdir}/out_*_${etag}_${weight}.yoda)"
      command="yodamerge -o ${subdir}_out_${weight}.yoda $(find ${subdir}/out_*_${etag}_${weight}.yoda)"
      echo ${command}
      eval ${command}
    fi
  done
done
cd ${CurrentDIR}
