CurrentDIR=$(pwd)
DIR=/afs/cern.ch/user/s/sthenkel/work/MCGenerators/RIVET/grid/download/2016_08_03-TopModellingStudies
#DIR=$CurrentDIR
#for subdir in $(ls -d ${DIR}/*/); do
for subdir in $(find $PWD -mindepth 1 -maxdepth 1 -type d); do
    echo into ${subdir}
    cd $subdir
    for tarball in *.XYZ.yoda.tgz*; do
	echo untaring ${tarball}
	tar -xzf ${tarball}
	prefix=$(echo "out_"${tarball} | awk -F .XYZ.yoda.tgz '{print $1}')
#	for yodafile in `find . -type f -exec grep -q *.yoda {} \; -not -exec grep -q out_*.yoda {} \; -print`; do
	for yodafile in `find $PWD -maxdepth 1 -type f -printf '%f\n' |  grep -v "out_" | grep -v "yoda.tgz" `; do
#	    echo 'DEBUG :: '$yodafile
	    mv ${yodafile} ${prefix}.${yodafile}
	done
    done
done
cd ${CurrentDIR}
