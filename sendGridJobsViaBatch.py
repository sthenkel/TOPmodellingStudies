#!/python
#
# author: Steffen Henkelmann
# example command : $> python sendGridJobsViaBatch --batch -c <shell script containing grid commands>

import glob,os,sys
import time,datetime

InputScript = sys.argv[1]

QUEUE = "atlasb1"
MEM_LIMIT="8000000"

ts = time.time()
timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H-%M')
gridcert = os.environ["X509_USER_PROXY"]
gridfile = str(gridcert).split("/",2)[2]

job=str(InputScript).split('_',2)
print '/n JOB name'
print '##############'
print '# >'+str(job[1])
job=job[1]

loc = os.environ["PWD"]+"/"
os.system("mkdir -p "+str(loc))
os.system("mkdir -p "+str(loc)+"gridcert")
os.system("cp "+str(gridcert)+" "+str(loc)+"gridcert/.")
os.system("mkdir -p "+str(loc)+"BatchJobHandler")
os.system("mkdir -p "+str(loc)+"BatchJobHandler/batchscripts")
os.system("mkdir -p "+str(loc)+"BatchJobHandler/batchscripts/"+str(job))
os.system("mkdir -p "+str(loc)+"BatchJobHandler/LSF")
os.system("mkdir -p "+str(loc)+"BatchJobHandler/LSF/"+str(job))

command = "source "+str(loc)+str(InputScript)
jobname = "batch_script_"+str(job)
# create submission script
script = open(str(loc)+"BatchJobHandler/batchscripts/"+str(job)+"/batch_script_"+str(job)+".lsf","w")
script.write("#BSUB -J "+str(jobname)+" \n")
script.write("#BSUB -o "+str(loc)+"BatchJobHandler/LSF/"+str(job)+"/"+str(jobname)+".log \n")
script.write("#BSUB -e "+str(loc)+"BatchJobHandler/LSF/"+str(job)+"/"+str(jobname)+".err \n")
script.write("#BSUB -q "+str(QUEUE)+"\n")
script.write("#BSUB -M "+str(MEM_LIMIT)+"\n")
script.write("\n")
script.write("cd "+str(loc)+" \n")
script.write("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh \n")
script.write("lsetup emi \n")
script.write("lsetup panda \n")
script.write("export X509_USER_PROXY="+str(loc)+"gridcert/"+str(gridfile)+" \n")

#script.write("source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh "+str(athenaVersion)+",here \n")
script.write(command+"\n")
script.write("cd - \n")
script.close()

print "Sending job "
print '###########################################################'
print command
print '###########################################################'
os.system("chmod 755 "+str(loc)+"BatchJobHandler/batchscripts/"+str(job)+"/"+str(jobname)+".lsf")
os.system("bsub -q "+str(QUEUE)+" < "+str(loc)+"BatchJobHandler/batchscripts/"+str(job)+"/"+str(jobname)+".lsf")
